#!/bin/bash

DST=./public/
MPCS="gi pruritus respiratory"
REGIONS="northwest east eastmidlands London northeast northernireland scotland wales westmidlands yorkshire"
ANIMALS="cat dog"

IMGROOT=https://fhm-chicas-storage.lancs.ac.uk/savsnet-agile-artefacts/public/consult_data/latest/

# jinja2 index.tmpl >$DST/index.html

## create separate region files:
# for region in $REGIONS ; do
#    REGION_MPC_FILE=${region}.html
#    jinja2 -D region=${region} region.tmpl info.json >$DST/$REGION_MPC_FILE
# done


## create separate animal files:

# for animal in $ANIMALS ; do
#     SPECIES_FILE=${animal}.html
#     jinja2 -D animal=${animal} national.tmpl > $DST/$SPECIES_FILE
#     for MPC in $MPCS ; do
# 	SPECIES_MPC_FILE=${animal}_${MPC}.html
# 	jinja2 -D animal=${animal} -D mpc=${MPC} mpc.tmpl info.json >$DST/$SPECIES_MPC_FILE
#     done
# done

jinja2 -D IMGROOT=$IMGROOT choose.tmpl info.json > $DST/index.html
cp choose.js $DST/choose.js
cp style.css $DST/style.css
