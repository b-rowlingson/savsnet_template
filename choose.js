console.log("loaded");
var checkboxes = document.querySelectorAll("input[type='checkbox']");
document.body.addEventListener('change', function (e) {
    let target = e.target;
    var regions = document.querySelectorAll("input.regions");
    var animals = document.querySelectorAll("input.animals");
    var mpcs = document.querySelectorAll("input.mpcs");
    
    for(var iregion=0; iregion<regions.length; iregion++){
	var showRegion = regions[iregion].checked;
	for(var ianimal=0; ianimal<animals.length; ianimal++){
	    var showAnimal = animals[ianimal].checked;
	    for(var impc=0 ; impc < mpcs.length; impc++){
		var showMpc = mpcs[impc].checked;
		var imgid = animals[ianimal].id +"_"+ mpcs[impc].id +"_"+ regions[iregion].id
		var img = document.getElementById(imgid)
		if(showMpc && showAnimal && showRegion){
		    img.style.display="block"
		}else{
		    img.style.display="none"
		}
	    }
	}
    };
});
